package com.ad.report

data class JiraIssue(val status: String, val originalEstimate: Int, val team: String) {
    fun remainingEstimate(): Int {
        return when("Closed - Complete"== status.trim()){
            true -> 0
            else -> originalEstimate
        }
    }
}