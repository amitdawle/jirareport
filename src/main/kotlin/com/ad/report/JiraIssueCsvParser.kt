package com.ad.report;

import java.util.stream.Stream

object JiraIssueCsvParser {

    fun parse(columnNames :List<String>, data: Stream<List<String>>) : Stream<JiraIssue> {
        val summary = getColumnIndex(columnNames, "Status")
        val originalEstimate = getColumnIndex(columnNames, "Original Estimate")
        val team = getColumnIndex(columnNames, "Team")
        return data.map{ arr -> JiraIssue(arr[summary], Integer.parseInt(arr[originalEstimate]), arr[team]) }
    }

    //Issue key,Issue id,Parent id,Summary,Status,Issue Type,Original Estimate,Priority,Team
    private fun getColumnIndex(columnNames : List<String>, columnName: String): Int{
        return columnNames.indexOf(columnName)
    }
}
