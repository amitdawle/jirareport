package com.ad.report

object TeamStatusFormat {

    fun format(statuses: List<TeamStatus>): List<List<String>> {
        return statuses.map { t ->
            listOf(t.team ,
                    String.format("%.6f", convertToJiraDay(t.originalEstimate)) ,
                    String.format("%.6f", convertToJiraDay(t.remainingEstimate))
            )
        }
    }

    private fun convertToJiraDay(timeInSeconds: Int): Double {
        return timeInSeconds * 1.0 / (8 * 60 * 60)
    }
}