package com.ad.report

data class TeamStatus(val team: String, val originalEstimate: Int, val remainingEstimate: Int)