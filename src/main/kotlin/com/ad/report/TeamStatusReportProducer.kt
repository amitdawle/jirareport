package com.ad.report

import java.util.stream.Collectors
import java.util.stream.Stream

object TeamStatusReportProducer {

    fun summarize(jiras: Stream<JiraIssue>) : List<TeamStatus> {
        val result: MutableMap<String, TeamStatus> =  jiras!!
                .collect(
                        Collectors.toMap({jiraIssue -> jiraIssue.team},
                                {jiraIssue -> TeamStatus(jiraIssue.team, jiraIssue.originalEstimate, jiraIssue.remainingEstimate()) },
                                {teamStatus1, teamStatus2 -> merge(teamStatus1, teamStatus2) }))
        return result.values.toList()
    }

    private fun merge(e1: TeamStatus, e2: TeamStatus) : TeamStatus {
            return TeamStatus(e1.team, e1.originalEstimate + e2.originalEstimate,
                    e2.remainingEstimate + e1.remainingEstimate)
    }
}

