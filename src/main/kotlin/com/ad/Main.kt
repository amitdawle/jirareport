package com.ad

import com.ad.csv.CSVReader
import com.ad.report.JiraIssueCsvParser.parse
import com.ad.report.TeamStatusReportProducer.summarize
import com.ad.report.TeamStatusFormat.format
import com.ad.csv.CSVWriter

fun main(args: Array<String>) {
    when(args.isEmpty()) {
        true -> println("Please provide a file name")
        else -> {
            val (columnNames, data) = CSVReader().with {
                                        separator(",")
                                        firstRowAsHeader()
                                    }
                                    .read(args[0])

            val jiraIssues = parse(columnNames, data)
            val summary = summarize(jiraIssues)
            val report = format(summary)

            val columns = listOf("Team, Total Effort, Remaining Effort")
            CSVWriter().
                    with{
                        headers(columns)
                        separator(",")
                        data(report)
                    }
                    .write()
        }
    }
}

