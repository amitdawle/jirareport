package com.ad.csv

class CSVWriter {

    private val columns = arrayListOf<String>()
    private var separator: String = ","
    private val body  = arrayListOf<List<String>>()

    fun with(init: CSVWriter.() -> Unit): CSVWriter {
        val writer = CSVWriter()
        writer.init()
        return writer
    }

    fun write(){
        println(this.columns.joinToString(separator))
        body.forEach{line -> println(line.joinToString(separator))}

    }

    fun headers(columns: List<String>) : CSVWriter {
        this.columns.addAll(columns)
        return this
    }

    fun data(data: List<List<String>>) : CSVWriter {
        this.body.addAll(data)
        return this
    }


    fun separator(separator: String) : CSVWriter {
        this.separator = separator
        return this
    }


}

