package com.ad.csv

import java.nio.file.Files
import java.nio.file.Paths
import java.util.stream.Stream

class CSVReader {

    var separator:String = ""
    var hasHeader        = true

    fun with(init: CSVReader.()-> Unit) : CSVReader {
        val reader = CSVReader()
        reader.init()
        return reader
    }

    fun firstRowAsHeader() : CSVReader {
        hasHeader        = true
        return this
    }

    fun noHeader() : CSVReader {
        hasHeader = false
        return this
    }

    fun separator(separator: String) : CSVReader {
        this.separator = separator
        return this
    }

    fun read(fileName : String) : Pair<List<String>, Stream<List<String>>> {
        val numberOfHeaderRows : Long = if (hasHeader) 1 else  0
        val columnNames = Files.lines(Paths.get(fileName))
                .map{h -> h.split(separator)}
                .limit(numberOfHeaderRows)
                .findFirst()
                .get()
        val data = Files.lines(Paths.get(fileName))
                .skip(numberOfHeaderRows)
                .map{s -> s.split(separator)}
        return Pair(columnNames, data)
    }

}